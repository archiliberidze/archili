<div class="row">
    <div class="col-md-12">
        <div class="form-group">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true" class="close-modal">&times;</span>
            </button>
        </div>
        <div class="form-group">
            <p class="user-info bold">User Info</p>

            <div class="border-gray user-info-radius container">
                <div class="row">
                    <div class="col-md-4">
                        <?php if(!empty($this->data['users']->image)){$img = $this->data['users']->image;}else{$img = 'face_1.jpg';} ?>
                        <div class="image-div">
                            <div class="image-vew">
                                <img src="<?php echo __JAMP__["images"]; ?>/users/<?php echo $img; ?>" class="user-info-image"/>
                            </div>
                        </div>
                        <p class="modal-username">
                            <span><?php echo(isset($this->data['users']->user_name) ? $this->data['users']->user_name : ''); ?> <?php echo(isset($this->data['users']->last_name) ? $this->data['users']->last_name : ''); ?></span>
                        </p>
                        <p class="modal-position">
                            <span><?php echo(isset($this->data['users']->positionName) ? $this->data['users']->positionName : ''); ?></span>
                        </p>
                    </div>
                    <div class="col-md-4 m-t-30 border-right-light-blue">
                        <div class="bg-light-blue font-size-14 details-user-info">
                            Active since <?php echo(isset($this->data['users']->active_since) ? $this->data['users']->active_since : ''); ?>
                        </div>

                        <div class="font-size-14 details-user-info">
                            Managers Group
                        </div>
                        <div class="bg-light-blue font-size-14 details-user-info">
                            Schedule: <?php echo(isset($this->data['users']->shcedule_title) ? $this->data['users']->shcedule_title : ''); ?>
                        </div>

                        <div class="font-size-14 details-user-info">
                            Next schedule:
                        </div>
                        <div class="bg-light-blue font-size-14 details-user-info">
                            Manages group:
                        </div>
                    </div>
                    <div class="col-md-4 m-t-30">
                        <div class="bg-light-blue font-size-14 details-user-info">
                            Phone
                            number <?php echo(isset($this->data['users']->phone_number) ? $this->data['users']->phone_number : ''); ?>
                        </div>

                        <div class="font-size-14 details-user-info">
                            Address: <?php echo(isset($this->data['users']->address) ? $this->data['users']->address : ''); ?>
                        </div>

                        <div class="bg-light-blue font-size-14 details-user-info">
                            Range allowance: <?php echo(isset($range) ? $range : ''); ?>
                        </div>

                        <div class="font-size-14 details-user-info">
                            Zip code: <?php echo(isset($this->data['users']->zip_code) ? $this->data['users']->zip_code : ''); ?>
                        </div>

                        <div class="bg-light-blue font-size-14 details-user-info">
                            Extratime: <?php echo(isset($settings->initial) ? $settings->initial : ''); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>