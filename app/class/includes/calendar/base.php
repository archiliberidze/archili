<div class="container">
    <div class="box-content col-md-12">
        <div>
            <ul class="calendarDayNames">
                <?php
                $weekDays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
                foreach($weekDays as $index => $label){
                    ?>
                    <li class="calendarDayName <?php echo $index == 0 || $index == 6 ? "calendarDayNameBlue" : ""; ?>"><?php echo $label; ?></li>
                    <?php
                }
                ?>
            </ul>
            <ul class="calendarDays">
                <?php

                   for ($a = $this->data['startYear']; $a <= $this->data['endYear']; $a++) {
                        $year = $a;
                        for ($c = 1; $c <= 12; $c++) {
                            if ($c >= 10) {
                                $month = $c;
                            } else {
                                $month = '0' . $c;
                            }

                            $currentMonth = date('m');

                            $currentDay = 0;
                            $daysInMonths = date('t', strtotime($year . '-' . $month . '-02'));
                            $numOfWeeks = ($daysInMonths % 7 == 0 ? 0 : 1) + intval($daysInMonths / 7);

                            $monthEndingDay = date('N', strtotime($year . '-' . $month . '-' . $daysInMonths));
                            $monthStartDay = date('N', strtotime($year . '-' . $month . '-02'));

                            if ($monthEndingDay < $monthStartDay) {
                                $numOfWeeks++;
                            }

                            $weeksInMonth = 6;
                            $nextMonth = null;
                            $previousMonthDays = 0;
                            $nextMonthDays = 0;

                            $previousMonth = $month - 1;
                            $previousYear = $year;

                            if ($previousMonth < 10) {
                                $previousMonth = '0' . $previousMonth;
                            }

                            if ($previousMonth == 0) {
                                $previousMonth = 12;
                                $previousYear = $previousYear - 1;
                            }

                            if ($previousMonth == 13) {
                                $previousMonth = 1;
                                $previousYear = $previousYear + 1;
                            }

                            $previousMonthDate = $previousYear . '-' . $previousMonth . '-02';

                            $daysInPreviousMonth = date('t', strtotime($previousMonthDate));
                            $thisMonthStartDay = date("w", strtotime($year . '-' . $month . '-01')) - 1;


                            for ($i = 0; $i < $weeksInMonth; $i++) {
                                $thisMonthStartRec = 0;
                                for ($b = 1; $b <= 7; $b++) {

                                    if ($currentDay == 0) {
                                        $firstDayOfTheWeek = date('N', strtotime($year . '-' . $month . '-02'));
                                        if (intval($i * 7 + $b) == intval($firstDayOfTheWeek)) {
                                            $currentDay = 1;
                                        }
                                    }

                                    if (($currentDay != 0) && ($currentDay <= $daysInMonths)) {
                                        $currentDate = date('Y-m-d', strtotime($year . '-' . $month . '-' . ($currentDay)));
                                        $cellContent = $currentDay;
                                        $currentDay++;
                                    } else {
                                        $currentDate = null;
                                        $cellContent = null;
                                    }

                                    if ($currentDay == 0 && $cellContent == "") {
                                        $previousMonthDays = $previousMonthDays + 1;
                                    }

                                    if ($currentDay == $daysInMonths + 1 && $cellContent == "") {
                                        $nextMonthDays = $nextMonthDays + 1;
                                    }
                                    ?>
                                    <li data-current-data="<?php echo $currentDate; ?>"
                                        class="calendarDaysNames <?php echo $currentMonth == $c && $year == date('Y', time()) ? '' : ' d-none ';
                                        echo $cellContent == "" ? ' lastDays ' : ''; ?> calendarMonth-<?php echo $c; ?>-<?php echo $year; ?>">
                                        <span class="calendarDayNumber">
                                            <?php
                                            if ($nextMonthDays != 0) {
                                                $thisMonthStartRec = 0;
                                                echo $nextMonthDays;
                                            } elseif ($previousMonthDays != 0 && $cellContent == "") {

                                                if ($thisMonthStartRec == 0) {
                                                    $thisMonthStartRec = $thisMonthStartDay;
                                                } else {
                                                    $thisMonthStartRec = $thisMonthStartRec - 1;
                                                }
                                                echo $daysInPreviousMonth - $thisMonthStartRec;
                                            } else {
                                                echo $cellContent;
                                            }
                                            ?>
                                        </span>
                                        <span class="calendarDayLunch">
                                            <span class="icon-lunch-times">
                                                <i class="icon-lunch-calendar"></i>
                                                <span class="icon-lunch-calendar-from">13:00</span>
                                                <span class="icon-lunch-calendar-duration">30 min</span>
                                            </span>
                                        </span>
                                        <span class="calendarDayTimes">09:00 - 18:00</span>
                                        <span class="calendarDayScheduleName">Sch. 4</span>
                                    </li>
                                    <?php
                                }
                            }
                        }
                    }
                ?>
            </ul>
        </div>
    </div>
</div>
