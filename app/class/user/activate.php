<?php

use system\jampModel;

if (!defined('__JAMP__')) {
    header('HTTP/1.0 404 Not Found');
    die();
}

class activate extends system\jampModel
{
    function index() {
        if(isset($this->url[1]) && $this->url[1] != ''){
            $this->data['success-code'] = $this->complicatedQuery(
                ['day'],
                'SELECT id FROM users WHERE verify = "'.$this->url[1].'"', '1'
            );

            if(!empty($this->data['success-code'])){
                $this->complicatedQuery(
                    ['day'],
                    'UPDATE users SET verify = "true" WHERE id = "'.$this->data['success-code']->id.'"', NULL, NULL, 'true'
                );
            }
        }
    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "frame.css",
                'login.css',
                'activate.css'
            ],
            'js' => [
                'activate.js'
            ],
            'plan' => [
                'activate',
            ]
        ];
        return $list;
    }


// if update day times create new record on dayrequest and create record in requestmessage


    public function getMenages () {
        return $this->customQuery("SELECT users FROM groups WHERE role = 1", 2);
    }

    public function getGroupMenage ($groupId=null){
	if (is_null($groupId) || empty($groupId)) return null;
        return $this->customQuery("SELECT id FROM users WHERE FIND_IN_SET($groupId, groups) AND _deleted = 0", 2);
    }

    public function updateDay()
    {
        $types = ['checkIn' => 'checkIn', 'checkOut' => 'checkOut', 'lunchStart' => 'lunchStart', 'lunchEnd' => 'lunchEnd'];
        $groupMenage = null;
        if (trim($this->post->message) == "" || !in_array($this->post->type, $types)) {
            return null;
        }

        $day = $this->complicatedQuery(
            ['day'],
            'SELECT day.userId, day.id, day_times.id AS dayTime
                        FROM day 
                            LEFT JOIN day_times ON day.id = day_times.dayId 
                    WHERE day._deleted = 0 AND day_times.id = ?', 1, [
                $this->post->updateDay
            ]
        );

        if (empty($day)) return null;

        $groupId = $this->complicatedQuery(['groups'], "SELECT id FROM groups WHERE FIND_IN_SET(?, users) ", true, [$day->userId]);

        if ($groupId) $groupMenage = $this->getGroupMenage($groupId->id);

        if (is_null($groupMenage) || empty($groupMenage)) $groupMenage = $this->getMenages();

        $dayRequest = $this->complicatedQuery(
            ['dayrequest'],
            'SELECT id FROM dayrequest WHERE dayId = ? AND '. $this->post->type .' IS NOT NULL AND fromUser = ? AND toUser IN ('.  $groupMenage[0] .') ',
            true, [
                $day->id, $day->userId
            ]
        );

        if ($dayRequest == false) {

            $status = "0";
            if ($this->post->urgent != "false") {
                $status .= ',3';
            }

            $groupMenage = explode(',', $groupMenage[0]);
            if (empty($groupMenage)) return null;

            /*foreach ($groupMenage as $key => $userId) {*/
                $reqId = $this->complicatedQuery(
                    ['dayrequest'],
                     'INSERT INTO dayrequest (dayId, dayTimeId, '.$this->post->type.', fromUser, toUser, status) VALUES (?, ?, ?, ?, ?, ?)',
                    1,[$this->post->day_id, $this->post->updateDay, $this->post->time, $this->session->userId, $groupMenage[0], $status],true);

                $this->complicatedQuery(
                    ['requestmessage'],
                    'INSERT INTO requestmessage (dayReqId, message, fromUser, toUser, reqId, sentTime) VALUES(?, ?, ?, ?, ?, CONVERT_TZ(NOW(), @@session.time_zone, ?))',
                    1, [
                        $this->post->day_id,
                        $this->post->message,
                        $this->session->userId,
                        $groupMenage[0],
                        $reqId,
                        $this->selectUserCompanyTimeZone($this->getSession('id'))
                    ], true);
            }
        /*}*/

        die();
    }

// if overtime checked its approved for paid else declined
    public function beforeExtratime()
    {
        $day = $this->selectRows('day', '*', '_deleted = 0 && id = "' . $this->post->dayId . '"', null, 1);

        $allMinutes = 0;
        if($this->post->before_extratime == 'decline'){
            $approve = 0;
        }else{
            $approve = 1;
            $allMinutes = $this->post->before_extratime;
        }


        if ($approve >= 1) {
            $log = $day->log . '\n Extra time approved (Before the shift)';
        } else {
            $log = $day->log . '\n Extra time canceled (Before the shift)';
        }
        $this->updateRecords('day', 'before_extratime = "' . $allMinutes . '", log = "' . $log . '"', $this->post->dayId . '');
    }

    public function afterExtratime(){
        $day = $this->selectRows('day', '*', '_deleted = 0 && id = "' . $this->post->dayId . '"', null, 1);

        $allMinutes = 0;
        if($this->post->after_extratime == 'decline'){
            $approve = 0;
        }else{
            $approve = 1;
            $allMinutes = $this->post->after_extratime;
        }


        if ($approve >= 1) {
            $log = $day->log . '\n Extra time approved (Afteк the shift)';
        } else {
            $log = $day->log . '\n Extra time canceled (After the shift)';
        }
        $this->updateRecords('day', 'after_extratime = "' . $allMinutes . '", log = "' . $log . '"', $this->post->dayId . '');
    }


    // sorting user by selected group
    public function sort_user_by_group()
    {if ($this->post->groupId == 0) {
        $groups = '';
    } else {
        $groups = ' AND FIND_IN_SET('.$this->post->groupId.',user_group.group_id)';
    }

        $this->data['users'] = $this->complicatedQuery(
            ['users', 'positions'],
            "SELECT users.*, users.id as table_user_id, users.name as user_name, users.last_name, positions.*, positions.id as positionId, positions.name as positionName, user_group.title as group_title
                    FROM users
                             LEFT JOIN ( SELECT users.id as user_id, GROUP_CONCAT(groups.id) as group_id, GROUP_CONCAT(groups.users) as group_users,
                                           GROUP_CONCAT(groups.title) as title
                                    FROM users
                                             LEFT JOIN groups ON FIND_IN_SET(users.id, groups.users)
                                    WHERE users._deleted = 0 GROUP BY users.id
                                  ) AS user_group ON user_group.user_id=users.id
                             LEFT JOIN ( SELECT users.id as user_id,
                                           positions.id as id,
                                           GROUP_CONCAT(positions.name) as name
                                    FROM users
                                             LEFT JOIN positions ON FIND_IN_SET(users.id, positions.users)
                                    WHERE users._deleted = 0 GROUP BY users.id
                    ) AS positions ON positions.user_id=users.id
                    WHERE users._deleted = 0 $groups
                    GROUP BY users.id ORDER BY users.last_name ASC"
        );


        $this->data['groups'] = $this->selectRows('groups', 'title, users, id', '_deleted = 0', null);
        $list['users'] = $this->select_user();
        print_r($list['users']);
    }

    function report_generation()
    {
        include "app/class/includes/report_function.php";
    }

    function select_user()
    {
        include "app/class/includes/select-group-users.php";
    }

    function user_info_modal()
    {
        include "app/class/includes/reports/user-info-modal.php";
    }

    function day_group_by_date()
    {
        include "app/class/includes/reports/day-group-by-date.php";
    }

    function day_group_by_employees()
    {
        include "app/class/includes/reports/day-group-by-employees.php";
    }

    function user_log_modal()
    {
        include "app/class/includes/reports/user_log_modal.php";
    }
}
