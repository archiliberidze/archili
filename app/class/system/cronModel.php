<?php 
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

use \PDO;
use system\jampDebug as debug;

abstract class cronModel {

	private $db			=   null;
	private $request    =   null;

    public $tables      =   null;

	public $config      =   null;

    private $_debug     =   null;
    public $_text       =   null;

    function __construct($debug, $config, $tables = null) {
        $this->_debug      =   $debug;
    	$this->config      =   $config;

    	#   MySQL Connection
        try {
            $this->db = new PDO(
                'mysql:host='.$this->config["host"].';dbname='.$this->config["name"].';charset=utf8', 
                $this->config["user"], 
                $this->config["pass"],
                [
                    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
                    PDO::ATTR_EMULATE_PREPARES => false,
                    PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
                ]
            );
        }
        catch( Exception $ex ) { die( $ex->getMessage() ); }

        //  #   Debug
        if( $this->_debug ){
            $debug          = new debug;
            $this->_text    = $debug->index();
        }

        if( $tables ){ 
            $this->tables = $tables; 
        }
        else{
            $this->fetchTables();
        }
    }

    function __destruct() {
        $this->db 			=   null;
        $this->_debug       =   null;
        $this->request 		=   null;
        $this->tables       =   null;
        $this->config 		=   null;
        $this->_text        =   null;
    }


    function fetchTables(){

        $this->request = $this->db->prepare( 'SHOW TABLES' );

        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . 'SHOW TABLES' . '<br><br>' ;

        $this->request->execute();

        $this->tables = $this->request->fetchAll(PDO::FETCH_COLUMN);
        return $this->tables;
    }

    function buildTable( $table = null, $sep = '-' ){
        $list = [
            $table,
            $table . $sep . $this->config['langList'][0]
        ];
        
        foreach ($list as $k => $v) {
            if( in_array($v, $this->tables) )return '`'.$v.'`';
        }

        return false;
    }

    function complicatedQuery( $tables = null, $query = null, $limit = null ){
    	if( $tables ){
            foreach ($tables as $key => $value) {
                $tables[$key] = $this->buildTable($value);
            }
        }

        for ($i = 0; $i < count($tables); $i++) { 
            $query = str_replace('{table'.$i.'}', $tables[$i], $query);
        }

        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

        $this->request = $this->db->prepare($query);
        $this->request->execute();
        if ($this->request) {
        	if($limit==true){
                return $this->request->fetch(PDO::FETCH_OBJ);
            }
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;

    }

    function selectRows( $table = null, $fields = null, $condition = null, $order = null, $origLimit = null){

        $limit = null;
        $table = $this->buildTable($table);

        if( $fields===null )    $fields = '*';
        if( $condition )        $condition = " WHERE " . $condition;
        if( $order )            $order = " ORDER BY " . $order;
        if( $origLimit )        $limit = " LIMIT " . $origLimit;

        $query = sprintf("SELECT %s FROM %s %s %s %s", $fields, $table , $condition, $order, $limit);
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

        $this->request = $this->db->prepare($query);
        $this->request->execute();
         
        if ($this->request) {
            if($origLimit==1){
                return $this->request->fetch(PDO::FETCH_OBJ);
            }
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    function unionSame( $table = null, $fields = null, $condition1 = null, $condition2 = null, $order = null, $limit = '' ){
    	$table = $this->buildTable($table);
    	if( $fields===null ){ $fields = '*'; }
    	if( $condition1 ) { $condition1 = " WHERE ".$condition1; }
    	if( $condition2 ) { $condition2 = " WHERE ".$condition2; }
    	if( $order ) { $order = " ORDER BY ".$order; }
    	if( $limit ) { $limit = " LIMIT ".$limit; }

    	$query = sprintf("(SELECT %s FROM %s %s %s %s) UNION (SELECT %s FROM %s %s %s %s)", $fields, $table , $condition1, $order, $limit, $fields, $table , $condition2, $order, $limit);
    	
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

    	$this->request = $this->db->prepare($query);
        $this->request->execute();
                     
    	if ($this->request) {
    		return $this->request->fetchAll(PDO::FETCH_OBJ);
    	}
    	return false;
    }

    function countRows( $table = '', $condition= '' ){
    	$table = $this->buildTable($table);
    	if( !empty($condition) ){
    		$condition = "WHERE " . $condition;
    	}
        $query = sprintf("SELECT COUNT(*) FROM %s %s" , $table , $condition);
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

        $this->request = $this->db->prepare($query);
    	$this->request->execute();
        if ($this->request) {
            return $this->request->fetchColumn(0);
        }
        return false;
    }
    function updateRecords($table = '', $set = '', $id = '') {
        if( !empty($table) && !empty($set) && !empty($id) ){
            $table = $this->buildTable($table);
            $query = sprintf("UPDATE %s SET %s WHERE `id` = %d;", $table, $set, $id);

            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

            $this->request = $this->db->prepare($query);
            $this->request->execute();

            if ($this->request) {
                return true;
            }
            return false;
        }else if( empty($table) && !empty($set) && empty($id) ){
            $query=$set;
            $this->request = $this->db->prepare($query);
            $this->request->execute();
            $this->request->rowCount();
        }
    }
    function insertRecordUni($table='', $fields='', $values='', $lastInsertId = false) {
        if( !empty($table) && !empty($fields) && !empty($values) ){
            $table = $this->buildTable($table);
            $query = sprintf("INSERT INTO %s (%s) VALUES (%s)", $table, $fields, $values);
            
            //  #   Debug
            if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

            $this->request = $this->db->prepare($query);
    		$this->request->execute();

            if( $lastInsertId ){
                return $this->db->lastInsertId();
            }

            if ($this->request) {
                return true;
            }
            return false;
        }
    }

    function groupBy( $table = null, $fields = null, $condition = null, $groupBy = null, $order = null ){
        $table = $this->buildTable($table);

        if( $fields===null ){ $fields = '*'; }
        if( $condition ) { $condition = " WHERE ".$condition; }
        if( $order ) { $order = " ORDER BY ".$order; }
        $groupBy =" GROUP BY ".$groupBy;

        $query = sprintf("SELECT %s FROM %s %s %s %s" , $fields, $table, $condition, $groupBy, $order);
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

        $this->request = $this->db->prepare($query);
        $this->request->execute();
        if ($this->request) {
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    function describeTable($table = null){
        $table = $this->buildTable($table);
        $query = sprintf( "DESCRIBE %s" , $table );
        
        //  #   Debug
        if( $this->_debug ) echo $this->_text['_debug'] . $this->_text['_debugQuery'] . $query. '<br><br>';

        $this->request = $this->db->prepare($query);
        $this->request->execute();
        if ($this->request) {
            return $this->request->fetchAll(PDO::FETCH_OBJ);
        }
        return false;
    }

    function customQuery( $query= '',$OBJ = true ){
        if( !empty($query) ){
            $this->request = $this->db->prepare($query);
            $this->request->execute();
        }
        if ($this->request) {
            if( $OBJ ){
                return $this->request->fetchAll(PDO::FETCH_OBJ);
            }
            else{
                return $this->request->fetchAll();
            }
        }
        return false;
    }

    function encording($value = null){
        if( $value ){
            $value = crypt($value, '$'.$this->config["algo"].'$rounds='.$this->config["roun"].'$'.$this->config["solt"].'');
            $value = base64_encode( $value );
            $value = md5($value);
            return $value;
        }
        return false;
    }

    function translate($key = null){
        if( isset($this->_text[$key]) ){
            return $this->_text[$key];
        }else{
            return 'No Translation: '.$key;
        } 
    }


}