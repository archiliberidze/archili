<div ng-app="myApp" ng-controller="myCtrl" class="container-fluid">
    <div class="row ">

        <div class="col-4">
            <span class="card">Balance (cents): {{user_info[0].balance}}   </span>
            <span class="card">Jackpot (cents):  {{jackpot}} </span>

            <div class="table1 ">
                <?php
                foreach ($this->data['bets'] as $bets)
                    echo "<div>$bets</div>";
                ?>

            </div>
        </div>
        <div class="col-4">
            <div class="flex-container" id="flex-container">

                <div class="outcome" id="outcome"></div>
            </div>
            <div class="roulette" id="roulette">
                <div class="wheel" id="wheel">
                    <div class="wrap" id="wrap">
                        <div class="ball" id="ball"></div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-4">
            <div class="card text-center ">
                <div>Bet History</div>
                <table class="table table-dark">
                    <thead>
                    <tr>
                        <th scope="col">Spin ID</th>
                        <th scope="col">Bet Time</th>
                        <th scope="col">Bet Amount</th>
                        <th scope="col">Win Amount</th>
                        <th scope="col">Win Number</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr ng-repeat="ui in user_info">
                        <td>{{ui.spin_id}}</td>
                        <td>{{ui.bet_time}}</td>
                        <td>{{ui.bet_amount}}</td>
                        <td>{{ui.win_amount}}</td>
                        <td>{{ui.win_number}}</td>

                    </tr>
                    </tbody>
                </table>
            </div>
        </div>

    </div>
    <div class="row">
        <div class="container">


        </div>
    </div>


</div>