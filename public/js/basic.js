let logout = {
    "tags" : {
        "userLogout" : ".user-logout",
        "logoutBox"  : ".logout-box"
    },
    "init" : function () {
        $(logout.tags.userLogout).click(function () {
            $(logout.tags.logoutBox).toggleClass('d-none');
        });
    },
    "autoLoad" : function () {
        logout.init();
    }
}

$(document).ready(function() {
    logout.autoLoad();
});