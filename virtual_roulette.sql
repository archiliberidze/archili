/*
 Navicat MySQL Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 100413
 Source Host           : localhost:3306
 Source Schema         : virtual_roulette

 Target Server Type    : MySQL
 Target Server Version : 100413
 File Encoding         : 65001

 Date: 16/08/2020 20:25:00
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;





-- able to handle thousands of simultaneous connections, requests from concurrent users

set global max_connections = 1000;

-- ----------------------------
-- Table structure for bet_history
-- ----------------------------
DROP TABLE IF EXISTS `bet_history`;
CREATE TABLE `bet_history`  (
                              `id` int(11) NOT NULL AUTO_INCREMENT,
                              `user_id` int(11) NULL DEFAULT 0,
                              `spin_id` int(11) NULL DEFAULT NULL,
                              `bet_time` timestamp(0) NULL DEFAULT current_timestamp(0),
                              `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `bet_amount` int(11) NULL DEFAULT NULL,
                              `win_amount` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                              `win_number` int(11) NULL DEFAULT NULL,
                              PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 119 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for jackpot
-- ----------------------------
DROP TABLE IF EXISTS `jackpot`;
CREATE TABLE `jackpot`  (
                          `id` int(11) NOT NULL AUTO_INCREMENT,
                          `jackpot` double(20, 2) NULL DEFAULT NULL,
                          PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of jackpot
-- ----------------------------
INSERT INTO `jackpot` VALUES (1, 9.35);

-- ----------------------------
-- Table structure for spin
-- ----------------------------
DROP TABLE IF EXISTS `spin`;
CREATE TABLE `spin`  (
                       `id` int(11) NOT NULL AUTO_INCREMENT,
                       `spin_time` timestamp(0) NULL DEFAULT current_timestamp(0),
                       `spin` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
                       `spin_id` int(11) NULL DEFAULT NULL,
                       `user_id` int(11) NULL DEFAULT NULL,
                       PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 124 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
                        `id` int(255) NOT NULL AUTO_INCREMENT,
                        `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                        `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                        `active_time` timestamp(0) NULL DEFAULT current_timestamp(0),
                        `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
                        `balance` bigint(255) NULL DEFAULT 0,
                        PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (8, 'achikoachi33@gmail.com', 'ace2a305057f53bea9c5613e6020bcbd', '2020-08-16 20:22:09', 'aberidze', 615);

-- ----------------------------
-- Triggers structure for table bet_history
-- ----------------------------
DROP TRIGGER IF EXISTS `bet_history`;
delimiter ;;
CREATE TRIGGER `bet_history` AFTER INSERT ON `bet_history` FOR EACH ROW BEGIN

  UPDATE jackpot
  SET jackpot=jackpot+NEW.bet_amount/100;

  UPDATE users
  SET users.balance=users.balance-NEW.bet_amount
  WHERE users.id=NEW.user_id;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table spin
-- ----------------------------
DROP TRIGGER IF EXISTS `spin`;
delimiter ;;
CREATE TRIGGER `spin` AFTER INSERT ON `spin` FOR EACH ROW BEGIN

  UPDATE users
  set active_time=NOW()
  WHERE users.id=NEW.user_id;

END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
