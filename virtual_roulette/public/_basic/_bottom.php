<?php if(!defined('__JAMP__')) exit( "Direct access not permitted." );?>

<?php
PHP_EOL;
foreach ($class->plan['js'] as $key => $value) {
	if( strpos($value, "https://") !== false || strpos($value, "http://") !== false){
		echo '	<script type="text/javascript" src="'. $value .'"></script>' . PHP_EOL;
	}
	else{
		echo '	<script type="text/javascript" src="'. __JAMP__['js'] . $value .'"></script>' . PHP_EOL;
	}
}
PHP_EOL;
?>
</body>
</html>