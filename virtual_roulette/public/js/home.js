const ball = $(".ball");
const wheel = $(".wheel")
const wrap = $(".wrap");
const title = $(".title");
const start = $(".start");
const bets = $("div.table1 > div");
let $input = $('.name');
let param= new Object();


var app = angular.module('myApp', []);


app.controller('myCtrl', function($scope) {


    class Selection {
        constructor(val) {
            this.val = val;
            this.$input = $('.name');
            this.$value = "";
        }



        //to start the game, click the start button and the table/wheel will be displayed and other text will also be display/hidden
        startGame() {
            start.click(function() {
                $(".p2").css("display", "none");
                $(".start").css("display", "none");
                $(".directions").css("display", "none");
                $(".name").css("display", "none");
                $(".wheel").css("display", "inline");
                $(".table1").css("display", "inline");
                $(".h2").css("display", "inline");
            });

            bets.click(function() {
               game.set_bet(this);
            });
        }


        // function to grab the information typed out in the form and use the value to display the players name once a number is selected
        inputForm() {
            const $button = $('button').click(this.grabInput)
            game.startGame()
        }


        //get user info
        user_info(){
            param= new Object();
            param.action="get_user_info";
            $.ajax({
                type: "POST",
                url: "home",
                dataType: 'json',
                data: param,
                success: function (data) {
                    $scope.$apply(function(){
                        $scope.user_info=data.user_info;
                    });
                }
            });
        }
        set_bet(x){
            param= new Object();
            param.action="set_bet";
            param.bet=$(x).text();
            $.ajax({
                type: "POST",
                url: "home",
                dataType: 'json',
                data: param,
                success: function (data) {
                    if(data.success!=null){

                        Swal.fire({
                            icon: 'success',
                            showConfirmButton: false,
                            text:  data.success,
                            timer: 1900
                        })
                        game.user_info();
                    }else {

                        Swal.fire({
                            icon: 'error',
                            showConfirmButton: false,
                            text:  data.error,
                            timer: 1900
                        })

                    }

                },
                error:function () {
                    location.reload();
                }
            });
        }
        jackpot(){
            param= new Object();
            param.action="get_jackpot";
            $.ajax({
                type: "POST",
                url: "home",
                dataType: 'json',
                data: param,
                success: function (data) {
                    $scope.$apply(function(){
                        $scope.jackpot=data.jackpot[0]['jackpot'];
                    });
                }
            });
        }




    }

    const game = new Selection();
    game.inputForm();
    game.user_info();
    game.jackpot();

    setInterval(function(){ game.jackpot(); }, 2000);


});