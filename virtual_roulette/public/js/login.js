

let login;
login = {
    "tags": {
        "loginGCaptcha"       : "login-gcaptcha",
        "loginKeyPress"       : ".login-email, .login-password",
        "signInButton"        : ".container .sign-in",
        "loginPassword"       : ".login-password",
        "password"            : ".password",
        "signIn"              : ".sign-in",
        "forgotPasswordLabel" : ".forgot-password-label",
        "loginEmail"          : ".login-email",
        "forgotEmail"         : ".forgot-email",
        "forgotNotification"  : ".forgot-notification",
        "forgotConfirm"       : ".forgot-confirm",
        "signInLabel"         : ".sign-in-label",
        "loginNotification"   : ".login-notification",
        "iconSuccess"         : ".icon.icon-success",
        "changePassword"      : ".change-password",
        "newPassword"         : "#new_password",
        "repeatPassword"      : "#repeat_password",
        "createAccount"       : ".createAccount",
        "register"            : ".register",
        "fieldsWrap"          : ".fields_wrap",
        "registerContainer"   : ".register-container",
        "fieldsWrapButtons"   : ".fields_wrap_buttons",
        "loginHere"           : ".loginHere"
    },
    "userLoginStatus": {
        1 : 'login',
        2 : 'recovery',
        3 : 'loggedOut',
        4 : 'passwordReset'
    },
    "errorsMessages": {
        "passwordsDoNotMatch" : "new password and repeat new password do not match.",
        "isEmpty"             : " field is empty.",
        "isNotEmail"          : " email is incorrect.",
        "isNotEmailRegister"  : "Email is incorrect.",
        "passwordNotMatching" : "Password and Repeat Password do not match."
    },
    "url": {
        "login": "login"
    },
    "codes": {
        "success": "000"
    },
    "types": {
        "login"          : "login",
        "forgot"         : "forgot",
        "changePassword" : "changePassword",
        "register"       : "register"
    },
    "siteKey"      : "6LfKVbUUAAAAAAk0EdwEv0ShSNI48OqpIfXwuzad",
    "theme"        : "light",
    "captcha"      : null,
    "captchaValue" : null,
    "email"        : null,
    "password"     : null,
    "errorCodes"   : null,
    "timeOut"      : 10000,
    "ajax": function (data, callBack) {
        $.ajax({
            url      : login.url.login,
            type     : 'post',
            dataType : 'json',
            data     : data,
            success  : callBack
        });
    },
    "showError": function (errorMessages) {
        $(login.tags.loginNotification).show();
        $(login.tags.loginNotification).text(errorMessages);

        /*setTimeout(function () {
            $(login.tags.loginNotification).empty();
            $(login.tags.loginNotification).hide();
        }, login.timeOut);*/
        return null;
    },
    "register" : {
        "tags" : {
            "registerusername"    : ".registerusername",
            "registerYourEmail"      : ".registerYourEmail",
            "registerPassword"       : ".registerPassword",
            "registerRepeatPassword" : ".registerRepeatPassword",
            "registerInput"          : ".registerInput",
            "errorMessage"           : ".errorMessage",
            "singUp"                 : ".singUp",
            "registerModal"          : "#registerModal",
            "register"               : ".register",
            "buttsWrap"              : ".butts_wrap",
            "registerMessage"        : ".register-message",
            "registerCount"          : ".register-count"
        },
        "error" : false,
        "messages" : "You have registered successfully",
        "loadInputs" : function () {
            $(login.register.tags.registerInput).each(function () {
                var cVal = $(this).val();

                if (cVal.trim() != "") {
                    login.register.error = false;
                    $(this).parent().find(login.register.tags.errorMessage).text("").addClass('d-none');

                } else {
                    let cName = $(this).attr('placeholder');
                    $(this).parent().find(login.register.tags.errorMessage).text(cName + login.errorsMessages.isEmpty).removeClass('d-none');

                    login.register.error = true;
                }
            });

            let email = $(login.register.tags.registerYourEmail);
            let emailError = email.parent().find(login.register.tags.errorMessage);

            if (login.register.emailTest(email.val())) {
                login.register.error = false;
                emailError.text('').addClass('d-none');
            } else {
                login.register.error = true;
                if (emailError.text().trim() == "") {
                    emailError.text(login.errorsMessages.isNotEmailRegister).removeClass('d-none');
                }
            }

            if ($(login.register.tags.registerPassword).val().trim() == $(login.register.tags.registerRepeatPassword).val().trim()) {
                login.register.error = false;
                $(login.register.tags.registerPassword).parent().find(login.errorsMessages.errorMessage).text('').addClass('d-none');
            } else {
                login.register.error = true;
                if ($(login.register.tags.registerPassword).parent().find(login.register.tags.errorMessage).text().trim() == "") {
                    $(login.register.tags.registerPassword).parent().find(login.register.tags.errorMessage).text(login.errorsMessages.passwordNotMatching).removeClass('d-none');
                }
            }

            if ($(login.register.tags.errorMessage).text().trim() == "") login.register.sendPost();
        },
        "save" : function () {
            $(login.register.tags.singUp).click(function () {
                login.register.loadInputs();
            });
        },
        "emailTest" : function (email) {
            var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            return re.test(String(email).toLowerCase());
        },
        "postCallBack" : function (data) {
            $(login.register.tags.registerModal).modal('show');
        },
        "startTimer" : function (duration, display) {
            var timer = duration, minutes, seconds;
            setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);

                minutes = minutes < 10 ? minutes : minutes;
                seconds = seconds < 10 ? seconds : seconds;

                display.text(seconds);

                if (--timer < 0) {
                    timer = duration;
                }
                if (seconds == 0) {
                    location.reload();
                }
            }, 1000);
        },
        "sendPost" : function () {

            $.post( login.url.login, {
                "type"           : login.types.register,
                "captcha"        : login.captcha,
                "username"    : $(login.register.tags.registerusername).val(),
                "yourEmail"      : $(login.register.tags.registerYourEmail).val(),
                "Password"       : $(login.register.tags.registerPassword).val(),
                "RepeatPassword" : $(login.register.tags.registerRepeatPassword).val()
            }).done(function(data) {
                login.register.successMessage();
            });
        },
        "successMessage" : function () {
            $(login.register.tags.register).addClass('d-none');
            $(login.register.tags.buttsWrap).addClass('d-none');
            $(login.register.tags.registerMessage).removeClass('d-none');
            login.register.startTimer(4, $(login.register.tags.registerCount));
        },
        "init" : function () {
            $(login.tags.createAccount + ' td span').click(function () {
                $(login.tags.loginHere).removeClass('d-none');
                $(login.tags.register).removeClass('d-none')
                $(login.tags.registerContainer).removeClass('d-none');

                $(login.tags.fieldsWrap).addClass('d-none');
                $(login.tags.fieldsWrapButtons).addClass('d-none');
                $(login.tags.createAccount).addClass('d-none');
            });

            $(login.tags.loginHere + ' td span').click(function () {
                $(login.tags.loginHere).addClass('d-none');
                $(login.tags.register).addClass('d-none')
                $(login.tags.registerContainer).addClass('d-none');

                $(login.tags.fieldsWrap).removeClass('d-none');
                $(login.tags.fieldsWrapButtons).removeClass('d-none');
                $(login.tags.createAccount).removeClass('d-none');
            });
        },
        "autoLoad" : function () {
            login.register.init();
            login.register.save();
        }
    },
    "testUserInput": function () {
        login.ajax({
            "captcha"  : login.captcha,
            "email"    : login.email,
            "type"     : login.types.login,
            "password" : login.password
        }, login.successCallBack);
    },
    "getUserInput": function () {
        login.captcha  = window.grecaptcha.getResponse(window.captcha);
        login.email    = $(login.tags.loginEmail).val();
        login.password = $(login.tags.loginPassword).val();
    },
    "successCallBack": function (response) {
        if (response) {
            login.errorCodes = jQuery.parseJSON(JSON.stringify(response));

            if (login.errorCodes['success'] !== undefined || login.errorCodes['passwordSuccessfullyChanged'] !== undefined ) {
                location.reload();
            } else {
                $(login.tags.loginNotification).show();
                let errorMessages = "";

                for (let key in login.errorCodes) {
                    if (login.errorCodes.hasOwnProperty(key)) {
                        errorMessages += login.errorCodes[key].result + '. ';
                    }
                }

                $(login.tags.loginNotification).text(errorMessages);

                if (login.errorCodes['recover'] !== undefined) {
                    $(login.tags.iconSuccess).removeClass('d-none');
                }

                /*setTimeout(function () {
                    $(login.tags.loginNotification).empty();
                    $(login.tags.loginNotification).hide();
                }, login.timeOut);*/
            }
        }
        window.grecaptcha.reset();
    },
    "captchaCallBack": function () {
        return window.grecaptcha.render(login.tags.loginGCaptcha, {
            'sitekey': login.siteKey,
            'theme'  : login.theme
        });
    },
    "loginListener": function () {
        $(login.tags.loginKeyPress).on('keypress', function (e) {
            if (e.which == 13) {
                $(login.tags.loginNotification).empty();
                $(login.tags.loginNotification).hide();
                login.init();
            }
        });

        $(login.tags.signInButton).click(function () {
            login.init();
            $(login.tags.loginNotification).empty();
            $(login.tags.loginNotification).hide();
        });
    },
    "init": function () {
        login.getUserInput();
        login.testUserInput();
    },
    "toggleLoginToForgot": function () {
        $(login.tags.loginPassword).toggle();
        $(login.tags.password).toggle();
        $(login.tags.signIn).toggle();
        $(login.tags.forgotPasswordLabel).toggle();
        $(login.tags.loginEmail).toggle();

        $(login.tags.forgotEmail).toggle();
        $(login.tags.forgotNotification).toggle();
        $(login.tags.forgotConfirm).toggle();
        $(login.tags.signInLabel).toggleClass('d-none');
    },
    "forgotPassword": {
        "checkEmail": function () {
            login.ajax({
                "type": login.types.forgot,
                "email": login.forgotPassword.email,
                "captcha": login.captcha
            }, login.successCallBack);
        },
        "init": function () {
            login.captcha = grecaptcha.getResponse(window.captcha);
            login.forgotPassword.email = $(login.tags.forgotEmail).val();
            login.forgotPassword.checkEmail();
        },
        "confirm": function () {
            $(login.tags.forgotEmail).on('keypress', function (e) {
                if (e.which == 13) {
                    $(login.tags.loginNotification).empty();
                    $(login.tags.loginNotification).hide();
                    login.forgotPassword.init();
                }
            });

            $(login.tags.forgotConfirm).on('click', function () {
                $(login.tags.loginNotification).empty();
                $(login.tags.loginNotification).hide();
                login.forgotPassword.init();
            });
        },
        "autoLoad": function () {
            login.forgotPassword.confirm();
        }
    },
    "changePassword": {
        "confirm": function () {
            $(login.tags.changePassword).on('keypress', function (e) {
                if (e.which == 13) {
                    $(login.tags.loginNotification).empty();
                    $(login.tags.loginNotification).hide();
                    login.changePassword.init();
                }
            });

            $(login.tags.changePassword).on('click', function () {
                $(login.tags.loginNotification).empty();
                $(login.tags.loginNotification).hide();
                login.changePassword.init();
            });
        },
        "init": function () {
            let password = login.changePassword.checkPassword();
            if (password == null) return;

            login.ajax({
                "type"     : 'resetPassword',
                "password" : $(login.tags.newPassword).val(),
                "captcha"  : login.captcha
            }, login.successCallBack);
        },
        "checkPassword": function () {
            let newPassword    = $(login.tags.newPassword).val();
            let repeatPassword = $(login.tags.repeatPassword).val();

            if ((newPassword == undefined && repeatPassword == undefined) || (newPassword == '' && repeatPassword == '')) {
                login.showError('new password and repeat new password fields are empty.');
                return;
            }

            newPassword    = newPassword.trim();
            repeatPassword = repeatPassword.trim();

            if (newPassword == '') {
                login.showError('new password field is empty.');
                return null;
            }

            if (repeatPassword == '') {
                login.showError('repeat new password field is empty.');
                return null;
            }

            if (newPassword != repeatPassword) {
                login.showError(login.errorsMessages['passwordsDoNotMatch']);
                return null;
            }

            return true;
        },
        "autoLoad": function () {
            login.changePassword.confirm();
        }
    },
    "autoLoad": function () {
        $(login.tags.signInLabel).on('click', login.toggleLoginToForgot);
        $(login.tags.forgotPasswordLabel).on('click', login.toggleLoginToForgot);

        login.register.autoLoad();
        login.forgotPassword.autoLoad();
        login.changePassword.autoLoad();
        login.loginListener();
    }
};

// loading call back for captcha
function captchaCallBack () {
    window.captcha = login.captchaCallBack();
}

$(document).ready(function() {
    login.autoLoad();
});