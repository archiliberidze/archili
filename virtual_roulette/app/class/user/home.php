<?php if (!defined('__JAMP__')) exit("Direct access not permitted.");
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ERROR);
class home extends system\jampModel
{

    function index()
    {
        $user_id = $this->session->userId;

        if (isset($this->post->action)) {
            switch ($this->post->action) {
                case 'set_bet':
                    $user_id =$this->session->userId;
                    $bet=html_entity_decode($this->post->bet);
                    $ip_address=$this->getUserIpAddr();


//Create Instance
                    $checkInstance =$this->CheckBets;


//Check Valid
                    $validateBet = $checkInstance->IsValid($bet);
                    $isValid = $validateBet->getIsValid();

                    if($isValid=='1'){
                        $betAmount = $validateBet->getBetAmount();

//Get Estimate Win
                        $winnum = rand(0,36);
                        $EstimateWin = $checkInstance->EstimateWin($bet, $winnum);



                        $user_spin_id=$this->complicatedQuery(null, "SELECT (IFNULL((SELECT max(spin_id) from spin WHERE user_id=$user_id),0)+1) as user_spin_id");
                        $last_user_spin_id=$user_spin_id[0]->user_spin_id;
                        $this->complicatedQuery(null, "INSERT INTO spin (spin,spin_id,user_id) 
                                                                     values ('$bet','$last_user_spin_id','$user_id') ");
                        $last_id=$this->complicatedQuery(null, "SELECT max(id) as last_spin_id from spin ");
                        $last_spin_id=$last_id[0]->last_spin_id;
                        $this->complicatedQuery(null, "INSERT INTO bet_history (user_id,spin_id,ip_address,bet_amount,win_amount,win_number) values ('$user_id','$last_spin_id','$ip_address','$betAmount','$EstimateWin','$winnum')");
                        $this->data['success']='bet was correct and accepted';
                    }else{
                        $this->data['error']='Is not valid bet';
                    }


                    break;

                case 'get_user_info':
                    $this->data['user_info'] = $this->complicatedQuery(null, "SELECT
                                                                                                users.balance AS balance,
                                                                                                spin.spin_id,
                                                                                                bet_history.bet_time,
                                                                                                bet_history.bet_amount,
                                                                                                bet_history.win_amount,
                                                                                                bet_history.win_number
                                                                                            FROM
                                                                                                users
                                                                                                LEFT JOIN bet_history ON bet_history.user_id = users.id
                                                                                                LEFT JOIN spin ON spin.id = bet_history.spin_id 
                                                                                            WHERE
                                                                                                users.id = '$user_id' 
                                                                                            ORDER BY
                                                                                                bet_history.bet_time DESC ");


                    break;

                case 'get_jackpot':

                    $this->data['jackpot'] = $this->complicatedQuery(null,"SELECT jackpot From jackpot limit 1");

                    break;

                default:
            }
            echo json_encode($this->data);
            die();
        }



        $file = fopen("bets.txt","r");

        while(! feof($file))
        {
            array_push($this->bets_array,fgets($file));
        }

        $this->data['bets']=array_filter($this->bets_array, function($value) { return $value!=''; });


        fclose($file);

    }

    function plan()
    {
        $list = [
            'css' => [
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css",
                "http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css",
                "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css",
                "home.css",
                "basic.css"
            ],
            'js' => [
                "https://code.jquery.com/jquery-3.4.1.min.js\" integrity=\"sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=\" crossorigin=\"anonymous",
                "https://code.jquery.com/ui/1.12.1/jquery-ui.js",
                "https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\" integrity=\"sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM\" crossorigin=\"anonymous",
                "https://ajax.googleapis.com/ajax/libs/angularjs/1.6.9/angular.min.js",
                "https://cdn.jsdelivr.net/npm/sweetalert2@9",
                "https://cdn.jsdelivr.net/npm/apexcharts",
                "home.js",
                "basic.js",
            ],
            'plan' => [
                'header',
                'home'

            ]
        ];
        return $list;
    }


}