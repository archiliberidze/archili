<?php
use system\jampModel;

if(!defined('__JAMP__')){header('HTTP/1.0 404 Not Found');die();}


class login extends system\jampModel {

    /*
     * json response codes (messages witch appears on login page).
     */
    private $responseCodes = [
        'success'                     => ['code' => '000', 'result' => 'Success'],
        'emailIsEmpty'                => ['code' => '001', 'result' => 'Email field is empty'],
        'passwordIsEmpty'             => ['code' => '002', 'result' => 'Password field is empty'],
        'invalidCaptcha'              => ['code' => '003', 'result' => 'Invalid captcha'],
        'emailIsNotAValid'            => ['code' => '004', 'result' => 'Email is not valid'],
        'invalidPassword'             => ['code' => '005', 'result' => 'Wrong password'],
        'invalidEmail'                => ['code' => '006', 'result' => 'Wrong email'],
        'recover'                     => ['code' => '007', 'result' => 'Check your email for recovery link'],
        'recoverIncorrect'            => ['code' => '008', 'result' => 'Recover link is incorrect or something went wrong, try again later'],
        'passwordNoUpperCaseSymbol'   => ['code' => '010', 'result' => 'Password does not contains uppercase letters'],
        'passwordNoLowerCaseSymbol'   => ['code' => '011', 'result' => 'Password does not contains lowercase letters'],
        'passwordNoNumbers'           => ['code' => '012', 'result' => 'Password does not contain a number'],
        'passwordNoSymbol'            => ['code' => '013', 'result' => 'Password does not contain symbols'],
        'passwordNoLessThen8'         => ['code' => '014', 'result' => 'Password is less than 8 letters'],
        'passwordSuccessfullyChanged' => ['code' => '015', 'result' => 'Password Successfully Changed'],
        'recoveryLinkSent'            => ['code' => '016', 'result' => 'Recovery link was already sent'],
    ];

    /*
     *  google captcha options
     */
    private $secretOptions =  [
        "ssl" => [
            "verify_peer" => false,
            "verify_peer_name" => false
        ]
    ];

    /*
     * redirect urls
     */
    private $urls = [
        'success' => 'requests',
        'recovery' => '/recovery/'
    ];

    /*
     * recovery email contents
     */
    private $emailRecoveryInfo = [ // needs some work when web page will be online.
        'subject' => 'Password Recovery',
        'txt' => '',
        'headers' => [
            'MIME-Version: 1.0' . '\r\n',
            'Content-type:text/html;charset=UTF-8' . '\r\n',
            'Cc: '
        ]
    ];

    /*
     * defines which page will load
     */
    public $currentUserStatus = null;

    /*
     * if in testing error is discovered if will be added to ths array so full json response can be returned.
     */
    private $errorList = [];

    /*
     * active when user login successfully.
     */
    private $success = null;

    /*
     * user info is selected from database using email.
     */
    private $userInfo = false;

    /* FOR TESTING ONLY........................................................................................................*/

        /* this will disable captcha for development (in production remove null and add true so captcha can work properly) */
        public $captcha = null;

    /* END FOR TESTING ONLY................................................................................................... */

    /**
     * @return void|null
     * testing if there is a post request.
     */
    public function index() {
        $this->data['userLoginStatus'] = $this->userLoginStatus;
        $this->getUserStatus();

        if (isset($this->page) && isset($this->url[1]) && $this->url[1] == 'recovery' && isset($this->url[2])) {
            $this->testRecoveryLink();
        }

        if(isset($this->post) && isset($this->post->type)) {
            if ($this->isUserLoggedIn()) {
                $this->setError('success');
                $this->returnResponse();
            }

            switch ($this->post->type) {
                case 'login' :
                        $this->loginPost();
                    break;
                case 'forgot' :
                        $this->forgotPost();
                    break;
                case 'resetPassword' :
                        $this->resetPassword();
                    break;
                case 'register' :
                    $this->register();
                    break;
            }
            die();
        } else {
            if ($this->isUserLoggedIn ()) {
                header("Location: {$this->urls['success']}");
            }
        }
    }

    /*
     * register user
     */
    private function register () {
        if (!isset($this->post->yourEmail)  || !isset($this->post->Password) || !isset($this->post->RepeatPassword)  || !isset($this->post->username)) return null;




        $this->complicatedQuery(["users"], "INSERT INTO users (email, username,  password)  VALUES (?,?, ?)", false, [
            $this->post->yourEmail, $this->post->username,  $this->encording($this->post->Password)
        ]);


//        $this->setSession('userStatus', $this->userLoginStatus['login']);
//        $this->setSession('userId', $userId);
//        $this->setSession('companyId',  $companyId);
//        $this->setSession('dbName',  $newDb);

        echo 'You have registered successfully.';
    }


    public function mailer(){


    }


    /**
     * tests recovery link
     */
    private function testRecoveryLink () {
        if (isset($this->url[1]) && $this->url[1] == 'recovery' && isset($this->url[2])) {
            $user = $this->complicatedQuery(['userrecovery'], 'SELECT userid, status FROM userrecovery WHERE recoverytoken = ?', true, [
                $this->url[2]
            ]);

            if (empty($user) || $user == false || ($user !== false && $user->status == 1)) return null;

            if (!empty($user) && $user->userid) {
                $this->setSession('userStatus', $this->userLoginStatus['passwordReset']);
                $this->setSession('userId', $user->userid);
                $this->getUserStatus();
            }
        }
    }

    /**
     * @return null|null
     * if user is not logged in, userStatus will be set as logged out
     */
    private function getUserStatus () {
        $userStatus = $this->getSession('userStatus');

        if (is_null($userStatus) && $userStatus !== $this->userLoginStatus['login']) {
            $this->currentUserStatus = $this->userLoginStatus['loggedOut'];
            $this->data['currentUserStatus'] = $this->userLoginStatus['loggedOut'];
            return null;
        } elseif (!isset($this->url[1]) && $userStatus == $this->userLoginStatus['recovery'] && $this->url[1] != 'recovery') {
            $this->data['currentUserStatus'] = $this->userLoginStatus['passwordReset'];
        }

        $this->currentUserStatus = $userStatus;
        $this->data['currentUserStatus'] = $userStatus;

        return null;
    }

    /**
     * @param null $userId
     * @return bool|null]
     *  test if a recovery token exists and if a new one is needed or user already has a recovery link
     */
    public function checkRecoveryLinkStatus($userId=null) {
        if (is_null($userId)) return null;

        $status = $this->complicatedQuery(['userrecovery'], 'SELECT status FROM userrecovery WHERE userId = ? AND status IS NULL', true, [
            $userId
        ]);

        if ($status == false || empty($status) || (!empty($status) && $status->status == 1)) return true;

        return false;
    }

    /**
     * changes password to user witch id is in the session
     */
    private function resetPassword () {
        $this->testIncomingPassword();

        $this->complicatedQuery(['users'], 'UPDATE users SET password = ? WHERE id = ?', null, [
            $this->encording($this->post->password), $this->getSession('userId')
        ]);

        $this->complicatedQuery(['userrecovery'], 'UPDATE userrecovery SET status = 1 WHERE userid = ?', null, [
            $this->getSession('userId')
        ]);

        $this->setError('passwordSuccessfullyChanged');
        return $this->returnResponse();
    }

    /**
     * @return null|null
     *  saves token in userrecovery table
     */
    private function saveToken ($userId=null, $recoveryToken=null) {
        if (is_null($userId) || is_null($recoveryToken)) return null;

        $this->complicatedQuery(['userrecovery'], 'INSERT INTO `userrecovery`(`recoverytoken`, `userid`) VALUES (?, ?)', null, [
            $recoveryToken, $userId
        ]);
    }

    /**
     * @return string
     * generates random string for recovery token
     */
    private function generateRecoveryToken() {
        return md5(uniqid(rand(), true));
    }

    /**
     * @param null $name
     * @return null|null
     * selects error codes from responseCodes and saves errors in errorList.
     */
    private function setError ($name=null) {
        if (is_null($name)) return null;

        $this->errorList[$name] = $this->responseCodes[$name];

        return true;
    }

    /**
     * password reset, (needs email to find user and sends a recovery link)
     */
    private function forgotPost() {
        if ($this->testIncoming() != true || $this->getUserInfo() != true) $this->returnResponse();

        if (empty($this->userInfo)) $this->getUserInfo();

        $tokenStatus = $this->checkRecoveryLinkStatus($this->userInfo->id);

        if ($this->userInfo == false || empty($this->userInfo)) {
            $this->setError('invalidEmail');
            $this->returnResponse();
        }

        if ($tokenStatus == false) {
            $this->setError('recoveryLinkSent');
            $this->returnResponse();
        }

        $token = $this->generateRecoveryToken();
        $this->saveToken($this->userInfo->id, $token);

        $recoveryLink = $this->page . $this->urls['recovery'] . $token;

        $this->setError('recover');
        $this->sendRecoveryLink($recoveryLink);
        $this->returnResponse();
    }

    /**
     * @param null $recoveryLink
     */
    private function sendRecoveryLink ($recoveryLink=null) {
        $this->sendEmail($this->post->email, $this->emailRecoveryInfo['subject'], $this->emailRecoveryInfo['txt'] . "<a href='{$recoveryLink}'>recovery link</a>");
    }

    /**
     * returns json response
     */
    private function returnResponse () {
        echo json_encode($this->errorList);

        die();
    }

    /**
     * @return bool|null
     * sets all of the user info in session.
     */
    private function setUserInfo () {
        if (!isset($this->userInfo) || $this->userInfo == false) return null; // just in case


        $userId = $this->complicatedQuery(['users'], 'SELECT * FROM `users` ', 1);
        $this->complicatedQuery(null, "UPDATE  `users` set active_time=NOW() where id='$userId->id'");
//        echo '<pre>';
//        print_r($userId);
//        echo '</pre>';
//        echo $db;
//        die();

        $this->setSession('userId',     $userId->id);
        $this->setSession('email',      $this->userInfo->email);
        //$this->setSession('username',   $this->userInfo->usename);
        $this->setSession('userStatus', $this->userLoginStatus['login']);


        $this->setError('success');
        $this->returnResponse();
    }

    /**
     * @return bool
     * gets user info from table uses using email. (and tests it, if the $this->userInfo is empty the email address is false and the user dos'n exists).
     */
    private function getUserInfo () {
        $this->userInfo = $this->complicatedQuery(['users'], 'SELECT * FROM users 
     
        WHERE  users.email= ?', null, [
            $this->post->email
        ]);

        if ($this->userInfo == false) {
            $this->setError('emailIsNotAValid');
            return false;
        }

        $this->userInfo = $this->userInfo[0];

        return true;
    }

    /**
     * @return void|null
     * testing if password is empty and not only...
     */
    protected function testIncomingPassword () {
        /*
         * 1. password needs to be one lowercase letters
         * 2. one uppercase letter
         * 3. one lowercase letter
         * 4. a symbol
         * 5. needs to have a number
         * 6. it needs to be longer then 8 letters
         */

        $this->post->password = trim($this->post->password);

        if (!preg_match('#[a-z]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoLowerCaseSymbol');
        }

        if (!preg_match('#[A-Z]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoUpperCaseSymbol');
        }

        if (!preg_match('#[0-9]+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoNumbers');
        }

        if (!preg_match('#\W+#', $this->post->password) && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoSymbol');
        }

        if (strlen($this->post->password) < 8 && isset($this->post->type) && $this->post->type == 'resetPassword') {
            $this->setError('passwordNoLessThen8');
        }

        if ($this->post->password === '' || !isset($this->post->password)) {
            $this->setError('passwordIsEmpty');
        }

        return !empty($this->errorList) ? $this->returnResponse() : null;
    }

    /**
     * @return bool
     * testing user input, if it is valid and not empty.
     */
    private function testIncoming () {
        if (!isset($this->post->email)) {
            $this->setError('emailIsEmpty');
        }



        !empty($this->errorList) ? $this->returnResponse() : null;

        if(!filter_var($this->post->email, FILTER_VALIDATE_EMAIL)) {
            $this->setError('emailIsNotAValid');
        }

        return !empty($this->errorList) ? false : true;
    }

    /**
     * collects all the logic in one function, (tests user input, gets user data and tests password).
     */
    private function loginPost () {
        if ($this->testIncoming() != true || $this->getUserInfo() != true) $this->returnResponse();

        $this->testIncomingPassword();

        $password = $this->encording($this->post->password);

        if($password !== $this->userInfo->password) {
            $this->setError('invalidPassword');
            $this->returnResponse();
        } else {
            $this->setUserInfo();
        }
    }

    /**
     * @param null $token
     * @return mixed|null
     * captcha config.
     */
    private function secret($token=null) {
        if (is_null($token)) return null;

        $secretKey = $this->config['secretKey'];

        return json_decode(
            file_get_contents(
                "https://www.google.com/recaptcha/api/siteverify?secret={$secretKey}&response={$token}",false,
                stream_context_create($this->secretOptions)
            )
        );
    }

    /**
     * @return array
     * basic plan for loading a page.
     */
    public function plan() {

        $list = [
            'css' => [
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
                'animate.css',
                "request/glyphter.css",
                'login.css'
            ],
            'js' => [
                'vue.js',
                'https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/solid.js',
                'https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js',
                'https://www.google.com/recaptcha/api.js?onload=captchaCallBack&render=explicit',
                'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js',
                'https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js',
                'login.js',
                'basic.js'
            ],
            'plan' => [
                'login'
            ]
        ];
        return $list;
    }
}