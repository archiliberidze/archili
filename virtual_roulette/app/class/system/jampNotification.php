<?php 
namespace system;

if(!defined('__JAMP__')) exit( "Direct access not permitted." );

class jampNotification {
	private $_debug				=   null;

//	private $_text				=   null;

	private $_location 			=	null;
	private $_message			=   null;

	function __construct(){
		$this->index();
	}

	function __destruct(){
		$this->_debug 			= null;
		$this->_text 			= null;
		$this->_location 		= null;
		$this->_message 		= null;
	}

	function index(){
	    if( $this->_message!==null ){
	        setcookie( 'message', $this->_text[$this->_message], time() + 5 );
	    }
	    header( "Location: ".$this->_location );
	    die();
	}
}

?>