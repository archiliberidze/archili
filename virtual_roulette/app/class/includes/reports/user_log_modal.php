<div class="form-group user-log-modal">
    <div>
        <div>
            <?php echo (isset($this->data['day'][0]->date)) ? date("d/m/Y", strtotime( $this->data['day'][0]->date)) : ''; ?>
        </div>
        <div>
            <span>Log Of</span>
                <span class="log-username"><?php echo (isset($this->data['user_id']->last_name)) ? $this->data['user_id']->name . ' ' . $this->data['user_id']->last_name : ''; ?></span>
        </div>
    </div>

    <div class="row m-t-10 p-b-10">
        <div class="col-12">
            <small class="m-l-85">From</small>
            <small class="m-l-35">To</small>
        </div>
        <?php
        foreach ($this->data['day'] as $item) {
            if(!empty($item->log_edit_time)) {
                if (!empty($item->checkIn)) {
                    $time = '<span class="min-width-80">Check in:</span> ' . $item->oldTime;
                    $newtime = $item->checkIn;
                } elseif (!empty($item->checkOut)) {
                    $time = '<span class="min-width-80">Check out:</span> ' . $item->oldTime;
                    $newtime = $item->checkOut;
                } elseif (!empty($item->lunchStart)) {
                    $time = '<span class="min-width-80">Lunch start:</span> ' . $item->oldTime;
                    $newtime = $item->lunchStart;
                } elseif (!empty($item->lunchEnd)) {
                    $time = '<span class="min-width-80">Lunch end:</span> ' . $item->oldTime;
                    $newtime = $item->lunchEnd;
                }
                ?>
                <div class="col-sm-5 log-border-right-dark left-text">
                <span class="d-block">
                    <?php echo $time; ?>
                    <img src="<?php echo __JAMP__["images"]; ?>/reports/blue_icon_from_to.svg" width="20px"/> <?php echo $newtime; ?>
                </span>
                </div>
                <div class="col-sm-7 m-t-2">
                    <i class="edit-by-log">Edited by <?php echo $this->data['user_info']->name.' '.$this->data['user_info']->last_name.' on <span class="default-blue-color">'.date("d.m.Y", strtotime(substr($item->log_edit_time, 0, 10))).'</span>'; ?></i>
                </div>
                <?php
            }
        }
        ?>
    </div>
</div>