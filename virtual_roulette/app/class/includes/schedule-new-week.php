<div class="row my-2 px-3 weeks-list" id="week-<?php echo $i; ?>-<?php echo $id; ?>">
    <div class="col-1 mx-2 sch-week-title"><?php echo $this->data['weekDay']; ?></div>

    <?php
        foreach ($this->data['weekDayId'] as $key => $id) {
        ?>
            <div class="col changeable noselect mx-2 py-1 add-time-to-day"
                 data-scheduleId="<?php echo $id; ?>"
                 data-clockIn=""
                 data-clockOut=""
                 data-workingDuration=""
                 data-range=""
                 id="shift-<?php echo $id; ?>"
                 data-shiftId="<?php echo $id; ?>">
                <span class="icon-r d-none"></span>
                <span class="icon-small-lunch d-none"></span>
                <span class="clockInClockOut">
                     --:-- - --:--
                </span>
            </div>
        <?php
        }

        $this->data['weekDayId'] = array_map(function($value) {
            return intval($value);
        }, $this->data['weekDayId'])
    ?>
    <div class="col noselect mx-2 py-1 schedule-week-remove" data-shiftId='<?php echo  json_encode(array_values($this->data['weekDayId'])); ?>'><i class="schedule-remove-icon"></i></div>
</div>
<script>
    $(function(){
        schedule.unbind();
        schedule.removeWeek();
        schedule.addWeek();
        schedule.addDayTime();
        schedule.openSchedule();
        schedule.dragAndDrops.autoLoad();
    });
</script>