<table class="table m-t-20 table-borderless table-hover table-users-all">
    <thead>
    <tr>
        <th>
            Name
            <div class="arrows">
                <span class="icon-arrow active-arrow"></span>
                <span class="icon-arrow arrow-down"></span>
            </div>
        </th>
        <th>
            Group
            <div class="arrows">
                <span class="icon-arrow"></span>
                <span class="icon-arrow arrow-down"></span>
            </div>
        </th>
        <th>
            Position
            <div class="arrows">
                <span class="icon-arrow"></span>
                <span class="icon-arrow arrow-down"></span>
            </div>
        </th>
        <th>
            Start Date
            <div class="arrows">
                <span class="icon-arrow"></span>
                <span class="icon-arrow arrow-down"></span>
            </div>
        </th>
        <th class="none-bold user-check-all center cursor-pointer">Select All</th>
    </tr>
    </thead>
    <tbody>
    <?php if (isset($this->data['users']) && !empty($this->data['users'])) { ?>
        <?php foreach ($this->data['users'] as $item) { ?>
            <tr>
                <td class="username-group" data-id="<?= $item->table_user_id; ?>"  data-toggle="modal" data-target="#exampleModalCenter-3"><?php echo(isset($item->last_name) ? $item->last_name : ''); ?> <?php echo(isset($item->user_name) ? $item->user_name : ''); ?></td>
                <td><?php echo(isset($item->group_title) ? $item->group_title : ''); ?></td>
                <td><?php echo(isset($item->positionName) ? $item->positionName : ''); ?></td>
                <td><?php echo (isset($item->active_since) ? substr($item->active_since, 0, 10) : ''); ?></td></td>
                <td class="center">
                    <div class="custom-control custom-checkbox custom-control-inline">
                        <input type="checkbox" class="custom-control-input user_reports_ids" name="user_ids[]"
                               value="<?= $item->user_id; ?>"
                               id="defaultInline1<?= $item->user_id; ?>">
                        <label class="custom-control-label my-checkbox"
                               for="defaultInline1<?= $item->user_id; ?>"></label>
                    </div>
                </td>
            </tr>
        <?php } ?>
    <?php } ?>
    </tbody>
</table>

<!-- Modal user info- -->
<div class="modal fade" id="exampleModalCenter-3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="user-info-modal modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" id="user-info">

            </div>
        </div>
    </div>
</div>

<script>
    $('.username-group').click(function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: "reports",
            // dataType: 'json',
            data: {
                username_user_id: $(this).attr('data-id'),
            },
            success: function (data) {
                if (!data) {
                    // vue.shakeAtWrong();
                } else {
                    document.getElementById('user-info').innerHTML = data;
                }
            }
        });
    });
</script>